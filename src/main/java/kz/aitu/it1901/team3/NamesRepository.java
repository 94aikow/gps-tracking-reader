package kz.aitu.it1901.team3;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class NamesRepository
{
    private final JdbcTemplate jdbcTemplate;

    public NamesRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Map<String, Object>> selectAllNames() {
        return this.jdbcTemplate.queryForList("select * from names");
    }
}