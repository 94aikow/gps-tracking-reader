package kz.aitu.it1901.team3;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@Component
public class NameController
{
    final
    NamesRepository namesRepository;

    public NameController(NamesRepository namesRepository) {
        this.namesRepository = namesRepository;
    }

    @GetMapping({ "/get-names" })
    public ArrayList<String> getNames() {
        List<Map<String, Object>> names = null;
        ArrayList<String> sortedNames = new ArrayList<>();
        try {
            names = this.namesRepository.selectAllNames();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println();
        if (names != null) {
            for (Map<String, Object> name : names) {
                String temp = (String) name.get("name");
                sortedNames.add(temp);
            }
        }
        Collections.sort(sortedNames);
        return sortedNames;
    }


}