FROM openjdk:12
ADD ./target/reader.jar reader.jar
ENTRYPOINT [ "java", "-Xmx64m", "-Xms64m", "-jar", "/reader.jar" ]
